package ru.t1.semikolenov.tm.repository.model;

import org.springframework.stereotype.Repository;
import ru.t1.semikolenov.tm.model.Project;

@Repository
public interface ProjectRepository extends AbstractUserOwnedRepository<Project> {

}
