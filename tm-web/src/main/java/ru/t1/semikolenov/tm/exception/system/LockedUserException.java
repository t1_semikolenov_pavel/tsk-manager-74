package ru.t1.semikolenov.tm.exception.system;

import ru.t1.semikolenov.tm.exception.AbstractException;

public final class LockedUserException extends AbstractException {

    public LockedUserException() {
        super("Error! User is locked...");
    }

}
